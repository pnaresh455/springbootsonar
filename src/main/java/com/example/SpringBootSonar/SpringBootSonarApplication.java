package com.example.SpringBootSonar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSonarApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootSonarApplication.class, args);
	}

}
